from django.shortcuts import render
from django.http import HttpResponse
from .models import Content
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


def manage_content(request, key):
    if request.method == "PUT":
        value = request.body.decode("utf-8")
        # If the key correspond to a existing content, update the value
        try:
            content = Content.objects.get(key=key)
            content.value = value
            content.save()
        # If the key dont corespond to a existing content, create a new content
        except Content.DoesNotExist:
            new_content = Content(key=key, value=value)
            new_content.save()


@csrf_exempt
def get_content(request, key):
    # Manage put
    if request.method == "PUT":
        manage_content(request, key)

    # Manage get
    try:
        content = Content.objects.get(key=key)
        return HttpResponse(content.value)
    except Content.DoesNotExist:
        return HttpResponse("Dosent exists a content for: " + key)
